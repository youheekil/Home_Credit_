
# Home Credit Default Risk
**Can you predict how capable each applicant is of repaying a loan?**

**Home Credit** trying to broaden financial inclusions for the unbanked
population who are actually trustworthy lenders by using statistical and machine
learning methods to predict capable
applicant of repaying a loan. Doing so will ensure that clients capable of
replayment are not rejected and that loans are given with a principal, muturity,
and repayment calenedar that will empower their clients to be successful.

Eventually, in this project, we will answer first question of `5 Questions Data Scientist Answers` will be
investigated and answered.

Check [Explainable AI](https://www.cc.gatech.edu/~alanwags/DLAI2016/(Gunning)%20IJCAI-16%20DLAI%20WS.pdf)

* **Is this A or B? (two-class classification)**  
> Is this applicant capable of repaying a loan or no?

To answer the question *Is this A or B* in this case, problems can be defined as
following:
1. Which factors will have a big impact on repaying the loan?
2. How would those factors affect the repayment ?

We are going to apply `Explainable Artificial Intelligence (XAI)` which refers to methods and techniques in the application of artificial intelligence such that the results of the solution can be understood by humans to answer the defined problems above [(wikipedia)](https://en.wikipedia.org/wiki/Explainable_artificial_intelligence)

----
1. Feature Engineering
2. Create Machine Learning Model
3. Find out which variable has the greatest impact through `shap value`
4. The relationship between the most influential five variables and whether to repay the loan.
----

### 1. Feature Engineering
### 2. Modelling
### 3. `shap value`
### 4. Check the relationship with the most influential five variables
